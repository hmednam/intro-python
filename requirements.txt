# to be used with:
# conda install --file requirements.txt
# utilities
python==3.8.5
spyder
ipython
spyder
jupyterlab
nbconvert>=5.5
bash_kernel
# base numerics
numpy
scipy
matplotlib
pandas
h5py
